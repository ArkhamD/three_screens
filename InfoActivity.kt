package com.example.threescreens

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity


class InfoActivity : AppCompatActivity() {
    private lateinit var text_id : TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)
        val first_name = intent.getStringExtra("first")
        val second_name = intent.getStringExtra("second")
        val third_name = intent.getStringExtra("third")
        val hobby = intent.getStringExtra("hobby")
        val age = intent.getStringExtra("age")

        text_id = findViewById(R.id.text_id)
        text_id.text = "Добрый день $first_name $second_name $third_name! \n" +
                "Мы очень рады, что вы в свои $age готовы заниматься вашим любимым $hobby!";
    }
}