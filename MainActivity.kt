package com.example.threescreens

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    private lateinit var btn_transfer: Button
    private lateinit var ed_first_name: EditText
    private lateinit var ed_second_name: EditText
    private lateinit var ed_third_name: EditText
    private lateinit var ed_age: EditText
    private lateinit var ed_hobby: EditText
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btn_transfer = findViewById(R.id.btn_transfer)
        ed_first_name = findViewById(R.id.ed_first_name)
        ed_second_name = findViewById(R.id.ed_second_name)
        ed_third_name = findViewById(R.id.ed_third_name)
        ed_age = findViewById(R.id.ed_age)
        ed_hobby = findViewById(R.id.ed_hobby)

        btn_transfer.setOnClickListener {
            intent = Intent(this, InfoActivity::class.java)
            intent.putExtra("first", ed_first_name.text.toString())
            intent.putExtra("second", ed_second_name.text.toString())
            intent.putExtra("third", ed_third_name.text.toString())
            intent.putExtra("age", ed_age.text.toString())
            intent.putExtra("hobby", ed_hobby.text.toString())
            startActivity(intent)
        }

    }
}